import React, { Component } from 'react';
import linkIcon from "./box-arrow-up-right.svg"
class PopularLocalities extends Component {
    render() {
        return (
            <div className='container mt-5'>
                <h1>Popular localities in and around Bengaluru</h1>
                <div className="row">
                    <div className="col-sm-4 mb-2">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">Indiranagar</h5>
                                <p className="card-text">1245 places</p>
                            </div>
                        </div>
                    </div>

                    <div className="col-sm-4 mb-2">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">Marathali</h5>
                                <p className="card-text">1854 places</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4 mb-2">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">Whitefield</h5>
                                <p className="card-text">   1861 Places</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4 mb-2">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">Kormangla 5th block</h5>
                                <p className="card-text">365 Places</p>
                            </div>
                        </div>
                    </div>

                    <div className="col-sm-4 mb-2">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">HSR</h5>
                                <p className="card-text">1554 places</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4 mb-2">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">Jaynagar </h5>
                                <p className="card-text">763 places</p>
                            </div>
                        </div>
                    </div>

                    <div className="col-sm-4 mb-2">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">JP Nagar</h5>
                                <p className="card-text">1209 places</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4 mb-2">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">Sharajapura Road</h5>
                                <p className="card-text">1245 places</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4 mb-2">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">See More</h5>
                                <p className="card-text"> <img src={linkIcon}/></p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PopularLocalities;
