import React, { Component } from 'react';


class Services extends Component {
    render() {
        return (
            <div className="container ">

                <div className="row mt-3">
                    <div className="col-lg-4 mb-4">
                        <div className="card">

                            <img src="https://b.zmtcdn.com/webFrontend/e5b8785c257af2a7f354f1addaf37e4e1647364814.jpeg?output-format=webp&fit=around|402:360&crop=402:360;*,*" className="card-img-top" alt="..." />
                            <div className="card-body">
                                <h5 className="card-title">Order Online</h5>
                                <p className="card-text">Stay at home and order at your doorstep</p>

                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 mb-4">
                        <div className="card">
                            <img src="https://b.zmtcdn.com/webFrontend/d026b357feb0d63c997549f6398da8cc1647364915.jpeg?output-format=webp&fit=around|402:360&crop=402:360;*,*" className="card-img-top" alt="..." />
                            <div className="card-body">

                                <h5 className="card-title no-gutters">Dinning</h5>
                                <p className="card-text">View city's favorite dining venues  </p>

                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 mb-4">
                        <div className="card">
                            <img src="https://b.zmtcdn.com/webFrontend/d9d80ef91cb552e3fdfadb3d4f4379761647365057.jpeg?output-format=webp&fit=around|402:360&crop=402:360;*,*" className="card-img-top" alt="..." />
                            <div className="card-body">
                                <h5 className="card-title">Night Life and Club</h5>
                                <p className="card-text">Explore the cities top nightlife outlets</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Services;
