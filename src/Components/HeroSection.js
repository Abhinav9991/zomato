import React, { Component } from 'react';
import "./HeroSection.css"

class HeroSection extends Component {
    render() {
        return (
            <div className=" container-fluid mask heroSection  ">
                <nav className="navbar navbar-expand-md navbar-dark bg-transparent" aria-label="Tenth navbar example">
                    <div className="container-fluid">
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse justify-content-around" id="navbarsExample08">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <a className="nav-link active" aria-current="page" href="#">Get the App</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Investor Relation</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link ">Add restaurant</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link ">Log in</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link ">Sign Up</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div className='d-flex flex-column  align-items-center h-50 text-light'>

                    <img className='title ' src='https://b.zmtcdn.com/web_assets/8313a97515fcb0447d2d77c276532a511583262271.png    ' />
                    <h1 className='my-5'>Discover the best food and drinks in Bengaluru </h1>
                    <div className="input-group d-flex justify-content-center row">
                        <div className="dropdown col-md-2    ">
                            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-bs-toggle="dropdown" aria-expanded="false">
                                Location
                            </button>
                            <ul className="dropdown-menu" aria-labelledby="dropdownMenu2">
                                <li><button className="dropdown-item" type="button">Kormangla</button></li>
                                <li><button className="dropdown-item" type="button">HSR Layout</button></li>

                            </ul>
                        </div>

                        <div className="form-outline col-md-3">
                            <input type="search" id="form1" placeholder='Search for a restaurant, cuisine or a dish' className="form-control" />
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default HeroSection;
