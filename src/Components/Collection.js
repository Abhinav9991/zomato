import React, { Component } from 'react';
import "./collection.css"
import linkIcon from "./box-arrow-up-right.svg"

class Collection extends Component {
    render() {
        return (
            <div className="container">
                <h1 className='text-start'>Collection</h1>

                <p className='text-start'><a className='link' href='#'> Explore curated lists of top restaurants, cafes, pubs, and bars in Bengaluru, based on trends <img src={linkIcon} /> </a></p>
                
                

                <div className="row">
                    <div className="col-lg-3 mb-4">
                        <div className="card">
                            <img src="https://b.zmtcdn.com/data/pictures/9/51239/699cdfb1db35ee4c3a377a6b3dae3643.jpg?output-format=webp&fit=around|771.75:416.25&crop=771.75:416.25;*,*" alt="" className="card-img-top" />
                            <div className="card-body">
                                <h5 className="card-title">Today's Specials</h5>
                                <p className="card-text">15 Places</p>

                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 mb-4">
                        <div className="card">
                            <img src="https://b.zmtcdn.com/data/pictures/6/20357196/b7ac28d2fa4d866181249a03b41fcab0.jpg?output-format=webp&fit=around|771.75:416.25&crop=771.75:416.25;*,*" alt="" className="card-img-top" />
                            <div className="card-body">
                                <h5 className="card-title">Newly Opened</h5>
                                <p className="card-text">16 Places</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 mb-4">
                        <div className="card">
                            <img src="https://b.zmtcdn.com/data/pictures/chains/8/51828/509b8f6ec56dc5cbf00d90a5ea3ce05c.jpg?output-format=webp&fit=around|771.75:416.25&crop=771.75:416.25;*,*60" alt="" className="card-img-top" />

                            <div className="card-body">
                                <h5 className="card-title">Trending this Week</h5>
                                <p className="card-text">16 Places</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 mb-4">
                        <div className="card">
                            <img src="https://b.zmtcdn.com/data/pictures/8/18578798/9a5f60042e083b7fed3711c9fcbfedb0_featured_v2.jpg?output-format=webp" alt="" className="card-img-top" />
                            <div className="card-body">
                                <h5 className="card-title">Best of Bengaluru</h5>
                                <p className="card-text">97 Places</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Collection;
