import React, { Component } from 'react';
import './Footer.css'

class Footer extends Component {
    render() {
        return (
            <div className='footer'>


                <div className="container ">
                    <footer className="py-5">
                        <div className="row">
                            <div className="col-md-3  ">
                                <h5 className='fw-bold'>About Zomato</h5>
                                <ul className="nav flex-column">
                                    <li className="nav-item mb-2 "><a href="#" className="nav-link p-0 text-light">Who We Are</a></li>
                                    <li className="nav-item mb-2 "><a href="#" className="nav-link p-0 text-light">Blog</a></li>
                                    <li className="nav-item mb-2 "><a href="#" className="nav-link p-0 text-light">Work With Us</a></li>
                                    <li className="nav-item mb-2 "><a href="#" className="nav-link p-0 text-light">Investors Relation</a></li>
                                    <li className="nav-item mb-2 "><a href="#" className="nav-link p-0 text-light">Report Fraud</a></li>
                                    <li className="nav-item mb-2 "><a href="#" className="nav-link p-0 text-light">Contact Us</a></li>
                                </ul>
                            </div>

                            <div className="col-md-3 ">
                                <h5 className='fw-bold'>ZOMAVERSE</h5>
                                <ul className="nav flex-column">
                                    <li className="nav-item mb-2"><a href="#" className="nav-link p-0 text-light">Zomato</a></li>
                                    <li className="nav-item mb-2"><a href="#" className="nav-link p-0 text-light">Feeding India</a></li>
                                </ul>
                            </div>

                            <div className="col-md-3  ">
                                <h5 className='fw-bold'>For Restaurant</h5>
                                <ul className="nav flex-column">
                                    <li className="nav-item mb-2"><a href="#" className="nav-link p-0 text-light">Partner With Us</a></li>
                                    <li className="nav-item mb-2"><a href="#" className="nav-link p-0 text-light">App For You</a></li>
                                    <li className="nav-item mb-2"><a href="#" className="nav-link p-0 text-light">Work With Us</a></li>
                                    <li className="nav-item mb-2"><a href="#" className="nav-link p-0 fw-bold text-dark">For Restaurant</a></li>
                                    <li className="nav-item mb-2"><a href="#" className="nav-link p-0 text-light">Zomato For Work</a></li>
                                </ul>
                            </div>
                            <div className="col-md-3  ">
                                <h5 className='fw-bold'>Learn More</h5>
                                <ul className="nav flex-column">
                                    <li className="nav-item mb-2"><a href="#" className="nav-link p-0 text-light">Terms</a></li>
                                    <li className="nav-item mb-2"><a href="#" className="nav-link p-0 text-light">Privacy</a></li>
                                    <li className="nav-item mb-2"><a href="#" className="nav-link p-0 text-light">Security</a></li>
                                    <li className="nav-item mb-2"><a href="#" className="nav-link p-0 text-light">Sitemap</a></li>
                                </ul>
                            </div>
                        </div>

                    </footer>
                </div>
            </div>
        );
    }
}

export default Footer;
