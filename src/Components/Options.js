import React, { Component } from 'react';
import "./Options.css"


class Options extends Component {
    render() {
        return (
            <div className='container mb-5 mt-5'>
                <h1 className='text-start'>Explore options near me . </h1>
                <div className="accordion" id="accordionExample">
                    <div className="accordion-item">
                        <h2 className="accordion-header" id="headingOne">
                            <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Popular cuisines near me
                            </button>
                        </h2>
                        <div id="collapseOne" className="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div className="accordion-body text-start ">
                                Bakery food near me .  Beverages food near me .  Biryani food near me . Burger food near me .  Chinese food near me . Desserts food near me . Ice Cream food near me . Juices food near me . Kebab food near me . Momos food near me . Mughlai food near me . North Indian food near me . Pizza food near me . Rolls food near me . Sandwich food near me . Seafood food near me . Shake food near me . Sichuan food near me . South Indian food near me . Street food near me .
                            </div>
                        </div>
                    </div>
                    <div className="accordion-item">
                        <h2 className="accordion-header" id="headingTwo">
                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Popular restaurant types near me
                            </button>
                        </h2>
                        <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div className="accordion-body">
                                Bakeries near me . Bars near me . Beverage Shops near me . Bhojanalya near me . Cafés near me . Casual Dining near me . Clubs near me . Cocktail Bars near me . Confectioneries near me . Dessert Parlors near me . Dhabas near me . Fine Dining near me . Food Courts near me . Food Trucks near me . Irani Cafes near me . Kiosks near me . Lounges near me . Microbreweries near me . Paan Shop near me . Pubs near me . Quick Bites near me . Sweet Shops near me .
                            </div>
                        </div>
                    </div>
                    <div className="accordion-item">
                        <h2 className="accordion-header" id="headingThree">
                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Top Restaurant Chains
                            </button>
                        </h2>
                        <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div className="accordion-body">
                                Biryani BluesBurger KingDomino'sKFCKrispy Kreme
                            </div>
                        </div>
                    </div>

                    <div className="accordion-item">
                        <h2 className="accordion-header" id="headingFour">
                            <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Cities We Deliver To
                            </button>
                        </h2>
                        <div id="collapseFour" className="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                            <div className="accordion-body">
                                Delhi NCR Kolkata Mumbai Bengaluru PuneHyderabad Chennai LucknowKochi Jaipur Ahmedabad Chandigarh Goa Indore Gangtok Nashik Ooty Shimla Ludhiana Guwahati Amritsar Kanpur Allahabad Aurangabad Bhopal Ranchi Visakhapatnam Bhubaneswar See More
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Options;
