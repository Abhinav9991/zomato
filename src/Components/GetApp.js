import React from 'react';
import "./GetApp.css"

const GetApp = () => {
    return (

        <div className='container d-flex  text-start'>
            <div className="card mb-3 border-0" >
                <div className="row g-0">
                    <div className="col-md-4">
                        <img src="https://b.zmtcdn.com/data/o2_assets/a500ffc2ab483bc6a550aa635f4e55531648107832.png" className="img-fluid rounded-start" alt="..." />
                    </div>
                    <div className="col-md-8">
                        <div className="card-body">
                            <h5 className="card-title">Get the Zomato App</h5>
                            <p className="card-text">We will send you a link, open it on your phone to download the app</p>
                        </div>
                        <div className='d-flex '>

                            <div className="form-check m-3">
                                <input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                                <label className="form-check-label" htmlFor="flexRadioDefault1">
                                    Email
                                </label>
                            </div>
                            <div className="form-check m-3 ">
                                <input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                                <label className="form-check-label" htmlFor="flexRadioDefault1">
                                    Phone
                                </label>
                            </div>
                        <div className="form-floating mb-3 w-50 d-flex ">
                            <input type="email" className="form-control me-3" id="floatingInput" placeholder="name@example.com" />
                            <label htmlFor="floatingInput">Email</label>
                            <button type="button" className="btn btn-success ps-4 pe-4 sendButton">send  </button>
                        </div>
                        </div>
                        <h3>Download App from </h3>
                        <div className='d-flex '>
                            <img className='playImage me-3' src="https://b.zmtcdn.com/data/webuikit/23e930757c3df49840c482a8638bf5c31556001144.png" />
                            <img className='playImage' src="https://b.zmtcdn.com/data/webuikit/9f0c85a5e33adb783fa0aef667075f9e1556003622.png" />

                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default GetApp;
