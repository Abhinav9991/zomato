import React, { Component } from 'react';

class NavBar extends Component {
    render() {
        return (
            <div className=" container-fluid navbar ">
                <div className="container  ">
                    <p className="navbar-brand text-dark">Get the App</p  >
                    <div className="d-flex text-dark" >
                        <p className='p-2'>Investor Relation</p>
                        <p className='p-2'>Add restaurant</p>
                        <p className='p-2'>Log in</p>
                        <p className='p-2 mr-4'>Sign up</p>
                        
                    </div>
                </div>
            </div>
        );
    }
}

export default NavBar;
