import './App.css';
import Collection from './Components/Collection';
import Footer from './Components/Footer';
import GetApp from './Components/GetApp';
import HeroSection from './Components/HeroSection';
import Options from './Components/Options';
import PopularLocalities from './Components/PopularLocalities';
import Services from './Components/Services';


function App() {
  return (
    <div className="App">
        <HeroSection/>
        <Services/>
        <Collection/>
        <PopularLocalities/>
        <Options/>
        <GetApp/>
        <Footer/>
        
    </div>
  );
}

export default App;
